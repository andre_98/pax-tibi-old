<div class="row">
	<div class="col-md-12">
		<div class="card mh-100">
			<div class="card-body">
				<p class="text-center"><img style="max-width:30%;" src="leonesanmarco.png"/></p>
				<p class="card-text"><strong>Pax tibi</strong>: pace a te. E' insieme un auspicio e una dichiarazione di intenti.<br>
	I padri della Serenissima vollero coniare la frase in latino, e benchè veneziani che ancora come loro parliamo il dialetto, intendiamo approfittare 
	di una lingua universale per conferire a queste due parole un significato che superi luoghi e razze.<br>
	Pax tibi è l'espressione del tempo di pace, ed infatti il Leone di San Marco, in tempo di guerra, brandisce la spada ed ha il libro chiuso .<br>
	Ed è la pace ciò che più di ogni altra aspirazione perseguì la Repubblica Serenissima; perchè è in tempo di pace che fioriscono i commerci,
	e con loro, l'arte, la cultura ed il benessere di tutti.<br>
	Il libro del Leone quando è aperto prosegue oltre l'augurio "pace a te" con le parole "Marce evangelista meo"; la frase è dunque indirizzata
	all'evangelista Marco, ed superfluo dire chi sia ritenuto l'autore.<br>  
	Ora, come un novello "Marco", chiunque si riconosca nell'augurio portato dal suo Leone è dunque invitato ad interpretare la "buona novella" 
	secondo la quale la pace è il bene superiore e per quanto difficile sia da conseguire, esso è benedetto da molto lontano.
	
			</div>	

		</div>
		<div class="card mh-100">
			<div class="card-body">
				<p class="card-text"><strong>Pax tibi</strong>: è un sito che rappresenta un gruppo di persone riunite nell'intento di 
				cooperare per migliorare le condizioni di vita del proprio territorio.<br>
				In questa prima fase, l'iniziativa è circoscritta alla zona di Favaro, Dese, Tessera, Campalto, Ca' Noghera e Ca' Solaro.</br> 
				Si pensava in prima battuta di mutuare le esperienze delle "misericordie", strutture di impegno sociale molto sviluppate nell'Italia
				centrale (vedi esempio:  <a href="http://www.misericordia.net/" target="_blank">Misericordia di Borgo San Lorenzo.</a>)<br>
				I recenti avvenimenti legati all'epidemia del Corona Virus, hanno tuttavia impresso un'accelerazione all'iniziativa per cui, 
				accantonate temporaneamente le operazioni di strutturazione dell'ente, ci si trova ora fronteggiare un'evenienza straordinaria 
				di fronte alla quale nessuno era preparato.<br>
				In questa prima fase pertanto aderiscono alcuni soggetti pubblici e privati nell'intento di coordinare gli interventi
				sul territorio; al momento aderiscono <a href="aderenti.php" target="_blank">(vedi aderenti)</a> secondo modalità volontaristiche
				del tutto destrutturate.  
	
			</div>	

		</div>
	</div>
</div>

<?php
	include('carousel.php');
?>
