<div class="row">
	<div class="col-md-12">
		<div class="card mh-100">
            <div class="card-body">
                <h1 class="text-center">Sostenitori</h1><br>
                <h3 class="text-center">Aziende, associazioni, soggetti privati che hanno dato un contributo alla gestione 
                dell'emergenza Covid 19</h3>
                <p class="card-text">
                    <h5>Imprese e associazioni.</h5>
                </p><br>

                <div class="card-footer">
                    <table style="width:100%">
                        <tr>
                            <td style ="width: 40%"card-footer">Pizzeria da Piero</td>
                            <td class="card-footer">
                                <a href="https://trattoriadapiero.it/" target="_blank" class="btn btn-success btn-block"> Vai </a>
                            </td>
                        </tr>
                        <tr>
                            <td>SNAMI Treviso</td>
                            <td>
                                <div class="card-footer">
                                    <a href="http://www.snamitreviso.org/home.html" target="_blank" class="btn btn-success btn-block"> Vai </a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Comune di Venezia -Controllo del Vicinato-</td>
                            <td>
                                <div class="card-footer">
                                    <a href="https://live.comune.venezia.it/it/2019/07/controllo-e-sorveglianza-di-vicinato-approvato-il-nuovo-logo-che-breve-sar-registrato" target="_blank" class="btn btn-success btn-block"> Vai </a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                        <td>Associazione Difesa dei Consumatori</td>
                            <td>
                                <div class="card-footer">
                                    <a href="https://www.associazionedifesaconsumatori.com/" target="_blank" class="btn btn-success btn-block"> Vai </a>
                                </div>
                            </td>
                        </tr>      
                    </table>                                 
                </div>
                     
                </br>
                <p class="card-text">
                    <h5>Privati cittadini.</h5>
                </p><br>

                <div class="card-footer">
                    <table style="width:100%">
                        <tr>
                            <td>Toni Buleghin</td>
                        </tr>
                        <tr>
                            <td>Nane dela Julia</td>
                        </tr>
                        <tr>
                            <td>Italo Rossi</td>
                        </tr>
                        <tr>
                            <td>Berto dela Pesa</td>
                        </tr>      
                    </table>                                 
                      <br>
                </div>
            </div>
        </div>
	</div>
</div>
<?php
?>
