<div class="row">
<div class="col">
	<div class="card">
		<div class="card">
			<div style="margin: 0 auto; width: 40%">
				<img src="images/coronavirus.jpg" class="card-img-center card" style="width:100%; margin:auto; margin-top:10px;">
				<div>
					<a href="#" data-toggle="modal" data-target="#sceglipercorsopaz" target="_self" class="btn btn-danger btn-block">info cittadini</a>
				</div>
				<div>
					<a href="#" data-toggle="modal" data-target="#richiestapassword" target="_self" class="btn btn-primary btn-block">info personale sanitario</a>
				</div>				
			</div>
		</div>
	</div> 
	<div class="card">
		<h3>Benvenuto nel sito Pax-tibi.org</h3><br>
		Il sito nasce in seguito all'emergenza sorta in seguito all'epidemia del corona virus. Data l'urgenza di fornire servizi
		ai cittadini e agli operatori socio sanitari, si tratta -come si vede- di un sito con una veste grafica essenziale e con 
		una struttura provvisoria che probabilmente verrà radicalmente riveduta in seguito. <br>
		L'intento del sito è quello di creare una rete di auto aiuto, per fornire informazioni a cittadini ed operatori socio-sanitari,
		per reclutare e coordinare le risorse disponibili e fornire aiuto materiale alle persone in difficoltà date le recenti disposizioni di legge.
		<br><br>
		L'auspicio è che l'emergenza coronavirus crei l'occasione per rinforzare i legami di vicinanza, solidarietà e corresponsabilità 
		già radicati nel nostro territorio e che la rete di buone relazioni persista oltre la durata dell'epidemia.
		<br><br>

	 	<div>
		 	<font size=2 face="verdana" color="green">
			...Dai diamanti non nasce niente, dal letame nascono i fior. <br>
			F. De Andrè
			</font>
		</div>	 		
	</div>	
	
</div>
